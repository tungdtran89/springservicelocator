package com.example.springservicelocator.services.validator.impl;

import com.example.springservicelocator.model.Message;
import com.example.springservicelocator.services.validator.MessageValidator;
import org.springframework.stereotype.Component;

@Component("SMS")
public class SMSMessageValidator implements MessageValidator {
    @Override
    public boolean validate(Message m) {
        //logic for validation
        return false;
    }
}
