package com.example.springservicelocator.services.validator;

public interface MessageValidatorFactory {
    MessageValidator getMsgValidator(String msgType);
}
