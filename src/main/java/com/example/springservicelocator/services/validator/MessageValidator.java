package com.example.springservicelocator.services.validator;

import com.example.springservicelocator.model.Message;

public interface MessageValidator {
    boolean validate(Message m);
}
