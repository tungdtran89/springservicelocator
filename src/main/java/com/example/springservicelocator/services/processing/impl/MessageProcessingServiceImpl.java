package com.example.springservicelocator.services.processing.impl;


import com.example.springservicelocator.model.Message;
import com.example.springservicelocator.services.processing.MessageProcessingService;
import com.example.springservicelocator.services.validator.MessageValidatorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessageProcessingServiceImpl implements MessageProcessingService {

    @Autowired
    private MessageValidatorFactory msgValidatorFactory;

    @Override
    public void processMessage(Message msg) {
        boolean isValid = msgValidatorFactory.getMsgValidator(msg.getType()).validate(msg);

        //do other processing
    }
}
