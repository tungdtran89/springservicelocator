package com.example.springservicelocator.services.processing;

import com.example.springservicelocator.model.Message;

public interface MessageProcessingService {
    void processMessage(Message msg);
}
