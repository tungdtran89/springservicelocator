package com.example.springservicelocator.model;

public class TelegramMessage extends Message {
    protected String sender;
    protected byte[] content;
    protected String recipient;
}
