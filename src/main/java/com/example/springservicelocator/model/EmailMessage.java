package com.example.springservicelocator.model;

import lombok.Data;

@Data
public class EmailMessage extends Message {
    protected String sender;
    protected byte[] content;
    protected String contentType;
    protected String recipient;
}
