package com.example.springservicelocator.model;

import lombok.Data;

@Data
public abstract class Message {
    protected String type;
}
