package com.example.springservicelocator.model;

import lombok.Data;

@Data
public class SMSMessage extends Message {
    protected String sender;
    protected String content;
    protected String recipient;
}
