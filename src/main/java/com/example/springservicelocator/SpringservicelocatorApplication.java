package com.example.springservicelocator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringservicelocatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringservicelocatorApplication.class, args);
	}

}
