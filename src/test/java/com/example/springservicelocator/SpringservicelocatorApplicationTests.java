package com.example.springservicelocator;

import com.example.springservicelocator.services.processing.MessageProcessingService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringservicelocatorApplicationTests {

	@Autowired
	private MessageProcessingService msgPrcSvc;

	@Test
	void contextLoads() {
		System.out.println("Hello world");
	}

}
